from fastapi import FastAPI, Path, Depends, status, HTTPException
import uvicorn
from pydantic import BaseModel
from db_config import session, engine
from class_model import Users
import traceback
from jose import JWTError, jwt
from datetime import datetime, timedelta

SECRET_KEY = "09d25e094faa6ca2556c818166b7a9563b93f7099f6f0f4caa6cf63b88e8d3e7"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

app = FastAPI()


def get_db():
    db = session()
    try:
        yield db
    finally:
        db.close()


class User(BaseModel):
    id: int
    email: str
    password: str
    name: str
    age: int


class login(BaseModel):
    email: str
    password: str

class Token(BaseModel):
    access_token: str
    token_type: str


def create_access_token(id=None):
    to_encode = {
        "id": id
    }

    # expire time of the token
    expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)

    # return the generated token
    return encoded_jwt


@app.get("/get_token")
async def get_token():
    # data to be signed using token
    data = {
        'info': 'secret information',
        'from': 'GFG'
    }
    token = create_access_token(data=data)
    return {'token': token}

# the endpoint to verify the token
@app.post("/verify_token")
async def verify_token(token: str):
    try:
        # try to decode the token, it will
        # raise error if the token is not correct
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        return payload
    except JWTError:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Could not validate credentials",
        )

@app.get("/")
async def main():
    return {"message": "Hello world"}


@app.post("/register")
def add_detail(b1: User, db: session = Depends(get_db)):
    add_data = Users(id=b1.id, email=b1.email, password=b1.password,
                     name=b1.name, age=b1.age)
    db.add(add_data)
    db.commit()
    # db.refresh(add_data)
    return Users(**b1.dict())


@app.post("/login")
def get_login(b1: login, db: session = Depends(get_db)):
    email = b1.email
    password = b1.password
    temp = []
    data = {
        'info': 'secret information',
        'from': 'GFG'
    }
    result = db.query(Users).filter(Users.email == email).first()
    id = result.id

    if result is None:
        return ("Something went wrong ")
    if result.password == password:
        token = create_access_token(id=id)
        temp.append({
            "id": result.id,
            "email": result.email,
            "name": result.name,
            "age": result.age,
            "token": token
        })
        return temp
    return ("Invalid credentials")

    # return {"message": "Successfully logged in"}


@app.get("/all_data")
def get_all(token: str, db: session = Depends(get_db)):
    try:
        data = db.query(Users).all()
        payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
        # if payload !=None:
        return data
    except:
        print(traceback.format_exc())
        return ({
        "message": "Something went wrong",
        "response_code": 401,
        })

@app.get("/get_id/{id}")
def get_id(token:str, id: int, db: session = Depends(get_db)):
    id = id
    id_result = db.query(Users).filter(Users.id == id).first()
    return id_result


# @app.post("/book")
# def add_book(book: Book):
#    data.append(book.dict())
#    return data


# @app.get("/book/{id}")
# def get_book(id: int):
#    id = id - 1
#    return data[id]
# @app.post("/hello/{name}/{age}/{add}")
# async def main_new(*, name: str=Path(...,min_lenghth=3, max_length=10), age, add):
#     return {"name": name, "age":age, "address":add}


if __name__ == "__main__":
    uvicorn.run("main:app", host="127.0.0.1", port=8000, reload=True)
